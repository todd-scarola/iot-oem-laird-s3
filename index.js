const SF = require('./sharedFunctions.js');

exports.handler = async(event) => {
    var responses = [];

    if (event.hasOwnProperty('Records'))
    {
        for (const record of event.Records) {
            //SNS event
            if (record.EventSource == 'aws:sns') {
                var body = JSON.parse(record.Sns.Message);
                if (body) { responses.push(await this.processMessage(body)); }
            }
        }
        return responses;
    }
    else
    //normal JSON request
    {
        return await this.processMessage(event);
    }
};

exports.processMessage = async(event) => {

    //get processing function
    var helper = null;
    if (event.hasOwnProperty('messageType')) {
        try {
            helper = require('./messages/' + event.messageType + '/main.js');
        }
        catch (error) {
            console.error(error);
        }
    }

    if (helper) {
        var message = await helper.processData(event);
        if (message) {
            console.log(message);
            return message;
        }
    }
};
