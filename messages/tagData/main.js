'use strict';
const SF = require('../../sharedFunctions.js');

exports.processData = async(event) => {

  // S3 scheme: Y/M/D/tags/<id>/oem/<messageType>/<time.json>
  var date = new Date(event.deviceTime * 1000);
  var path = 'Laird/MG100/tagData/' + await SF.getS3Path(date) + '/deviceId-' + event.deviceId + '/' + date.getTime() + '.json';

  //exports.saveToS3 = async function(bucket, key, metadata, data)
  var result = await SF.saveToS3(process.env.S3_BUCKET, path, null, JSON.stringify(event));
  if (result) {
    return process.env.S3_BUCKET + '/' + path;
  }
  else {
    console.error('error writing to S3: %j', result);
  }

};

/*
{
  "entryProtocolVersion": 2,
  "deviceId": "fa0ca0ec8ba7",
  "deviceTime": 1605799882,
  "lastUploadTime": 1605795629,
  "fwVersion": "00020800",
  "batteryLevel": 2944,
  "networkId": 65535,
  "entries": [
    {
      "entryStart": 165,
      "flags": 253,
      "scanInterval": 30,
      "serial": "e9ed73c7708a",
      "timestamp": 1605795682,
      "length": 256,
      "logs": [
        {
          "recordType": 17,
          "log": {
            "delta": 0,
            "rssi": -38,
            "motion": 0,
            "txPower": 0
          }
        },
        {
          "recordType": 17,
          "log": {
            "delta": 3,
            "rssi": -41,
            "motion": 0,
            "txPower": 0
          }
        },
        {
          "recordType": 17,
          "log": {
            "delta": 4,
            "rssi": -36,
            "motion": 0,
            "txPower": 0
          }
        },
        {
          "recordType": 17,
          "log": {
            "delta": 5,
            "rssi": -36,
            "motion": 0,
            "txPower": 0
          }
        },
        {
          "recordType": 17,
          "log": {
            "delta": 6,
            "rssi": -38,
            "motion": 0,
            "txPower": 0
          }
        },
        {
          "recordType": 17,
          "log": {
            "delta": 7,
            "rssi": -36,
            "motion": 0,
            "txPower": 0
          }
        },
        {
          "recordType": 17,
          "log": {
            "delta": 8,
            "rssi": -36,
            "motion": 0,
            "txPower": 0
          }
        },
        {
          "recordType": 17,
          "log": {
            "delta": 9,
            "rssi": -36,
            "motion": 0,
            "txPower": 0
          }
        },
        {
          "recordType": 17,
          "log": {
            "delta": 10,
            "rssi": -36,
            "motion": 0,
            "txPower": 0
          }
        },
        {
          "recordType": 17,
          "log": {
            "delta": 11,
            "rssi": -37,
            "motion": 0,
            "txPower": 0
          }
        },
        {
          "recordType": 17,
          "log": {
            "delta": 12,
            "rssi": -41,
            "motion": 0,
            "txPower": 0
          }
        },
        {
          "recordType": 17,
          "log": {
            "delta": 13,
            "rssi": -36,
            "motion": 0,
            "txPower": 0
          }
        },
        {
          "recordType": 17,
          "log": {
            "delta": 14,
            "rssi": -36,
            "motion": 0,
            "txPower": 0
          }
        },
        {
          "recordType": 17,
          "log": {
            "delta": 15,
            "rssi": -39,
            "motion": 0,
            "txPower": 0
          }
        },
        {
          "recordType": 17,
          "log": {
            "delta": 16,
            "rssi": -40,
            "motion": 0,
            "txPower": 0
          }
        },
        {
          "recordType": 17,
          "log": {
            "delta": 17,
            "rssi": -36,
            "motion": 0,
            "txPower": 0
          }
        },
        {
          "recordType": 17,
          "log": {
            "delta": 18,
            "rssi": -37,
            "motion": 0,
            "txPower": 0
          }
        },
        {
          "recordType": 17,
          "log": {
            "delta": 19,
            "rssi": -41,
            "motion": 0,
            "txPower": 0
          }
        },
        {
          "recordType": 17,
          "log": {
            "delta": 20,
            "rssi": -37,
            "motion": 0,
            "txPower": 0
          }
        },
        {
          "recordType": 17,
          "log": {
            "delta": 21,
            "rssi": -36,
            "motion": 0,
            "txPower": 0
          }
        },
        {
          "recordType": 17,
          "log": {
            "delta": 23,
            "rssi": -36,
            "motion": 0,
            "txPower": 0
          }
        },
        {
          "recordType": 17,
          "log": {
            "delta": 24,
            "rssi": -36,
            "motion": 0,
            "txPower": 0
          }
        },
        {
          "recordType": 17,
          "log": {
            "delta": 25,
            "rssi": -37,
            "motion": 0,
            "txPower": 0
          }
        },
        {
          "recordType": 17,
          "log": {
            "delta": 26,
            "rssi": -36,
            "motion": 0,
            "txPower": 0
          }
        },
        {
          "recordType": 17,
          "log": {
            "delta": 27,
            "rssi": -36,
            "motion": 0,
            "txPower": 0
          }
        },
        {
          "recordType": 17,
          "log": {
            "delta": 28,
            "rssi": -36,
            "motion": 0,
            "txPower": 0
          }
        },
        {
          "recordType": 17,
          "log": {
            "delta": 29,
            "rssi": -36,
            "motion": 0,
            "txPower": 0
          }
        },
        {
          "recordType": 17,
          "log": {
            "delta": 30,
            "rssi": -39,
            "motion": 0,
            "txPower": 0
          }
        },
        {
          "recordType": 17,
          "log": {
            "delta": 31,
            "rssi": -36,
            "motion": 0,
            "txPower": 0
          }
        },
        {
          "recordType": 17,
          "log": {
            "delta": 32,
            "rssi": -37,
            "motion": 0,
            "txPower": 0
          }
        }
      ]
    }
  ],
  "gatewayId": "354616090321758",
  "messageType": "tagData"
}
*/
